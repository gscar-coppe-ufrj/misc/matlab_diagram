MATLAB Diagram

This repository is public. Your collaboration is welcomed.

Features:
- All strings are interpreted as latex.
- Create Blocks and Circles with a text centralized.
- Create Ports on the blocks and circles.
    - Ports are aligned on the left, right, top and down on the block.
    - Ports are aligned on the circle border.
    - Each port has a text associated.
    - The user can add as many ports as necessary.
- Create Arrows 
    - Arrow is a set of points
    - The arrow head can be on the start and/or the end.
    - Can be draw with a spline
    - The position of the ports on blocks and circles can be used to set the arrow position.
- (X) Create AutoArrow (not implemented yet)
- Configure the properties of those elements (block/circle/arrow) individually or the globally.
- The result is a beautiful image that can be saved.
    - Eg: print('filename','-dpdf') to save as pdf.
    - export_fig is nice (https://www.mathworks.com/matlabcentral/fileexchange/23629-export-fig)

Installation Instructions:
- Download/clone the repository into any directory "(dir)".
- In MATLAB, add folder "(dir)/src" into the paths.
- Run "(dir)/test.m" to test the library. A figure will be generated with a few blocks and circles and arrows connecting them.

