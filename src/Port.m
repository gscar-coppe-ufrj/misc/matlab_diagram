classdef Port < handle
    %PORT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        txt
        pos
        shiftTxt
        
        fontSize
        fontColor
        horizontalAlignment
        verticalAlignment
    end
    
    methods
        function obj = Port(varargin)
            if nargin > 0
                if isa(varargin{1}, 'Port') % copy constructor
                    p = varargin{1};
                    obj.txt = p.txt;
                    obj.pos = p.pos;
                    obj.shiftTxt = p.shiftTxt;
                    obj.fontColor = p.fontColor;
                    obj.fontSize = p.fontSize;
                    obj.horizontalAlignment = p.horizontalAlignment;
                    obj.verticalAlignment = p.verticalAlignment;
                    
                else % normal constructor
                    obj.txt = varargin{1};
            
                    obj.pos = [0 0 0];
                    obj.shiftTxt = [0 0];

                    global diagram;

                    obj.fontColor = diagram.portFontColor;
                    obj.fontSize = diagram.portFontSize;
                    obj.horizontalAlignment = diagram.portHorizontalAlignment;
                    obj.verticalAlignment = diagram.portVerticalAlignment;
                end
            end
        end
        
        function Print(obj)
            yLimits = get(gca,'YLim');
            
            p = obj.pos + [obj.shiftTxt 0 ];
            t = text(p(1), p(2), obj.txt, 'Interpreter', 'latex', 'Clipping', 'on', 'FontUnits', 'normalized');
            t.HorizontalAlignment = obj.horizontalAlignment;
            t.VerticalAlignment = obj.verticalAlignment;
            t.FontSize = obj.fontSize/(yLimits*[-1; 1]);
            t.Color = obj.fontColor;
        end
    end
    
end

