classdef AutoArrow < Arrow
    %SEGMENTEDARROW Summary of this class goes here
    %   Detailed explanation goes here
    
%     properties (Access = private)
    properties
        segments
        directionFlags
        quadrant1
        quadrant2
        quadrant3
        quadrant4
        allowedDirections = {'Up', 'Left', 'Down', 'Right'}
        extraDisplacementsPercent
    end
    
    methods
        function obj = AutoArrow(varargin)
            
            if isa(varargin{1},'AutoArrow') %Copy constructor
                args = varargin(1);
            else %Normal constructor
                args = varargin(1:2);
            end
            obj = obj@Arrow(args{:});
            
            %Copy constructor
            if isa(varargin{1},'AutoArrow')
                sourceObj = varargin{1};
                obj.directionFlags = sourceObj.directionFlags;
                obj.quadrant1 = sourceObj.quadrant1;
                obj.quadrant2 = sourceObj.quadrant2;
                obj.quadrant3 = sourceObj.quadrant3;
                obj.quadrant4 = sourceObj.quadrant4;
                obj.extraDisplacementsPercent = sourceObj.extraDisplacementsPercent;
            
            %Normal constructor
            else
                if nargin > 3
                    obj.extraDisplacementsPercent = zeros(1,length(varargin)-2);
                    for i = 1:(length(varargin)-2)
                        obj.extraDisplacementsPercent(i) = varargin{i+2};
                    end
                else
                    obj.extraDisplacementsPercent = [];
                end

                obj.directionFlags = [];
                obj.quadrant1 = [ 0 pi/2 ];
                obj.quadrant2 = [ pi/2 pi ];
                obj.quadrant3 = [ pi 3*pi/2 ];
                obj.quadrant4 = [ 3*pi/2 2*pi ];

                obj.points = [varargin(1) varargin(2)];
                middlePoints = obj.ComputeMiddlePoints(obj.points{1}, obj.points{end}, obj.extraDisplacementsPercent);
                obj.points = [obj.points(1) middlePoints obj.points(end)];
            end
        end
        
        function middlePoints = ComputeMiddlePoints(obj, pStart, pEnd, varargin)
            
            global diagram;
            if ~isempty(varargin{1})
                obj.extraDisplacementsPercent = varargin{1};
            else
                obj.extraDisplacementsPercent = [];
            end
            
            % Source and target angles
            if length(pStart) < 3
                sourceAng = 0;
            else
                sourceAng = pStart(3);
            end
            if length(pEnd) < 3
                targetAng = 0;
            else
                targetAng = pEnd(3);
            end
            
            % First and end points, obtained from source and target angles
            firstPoint = pStart(1:2) + (obj.extraPointsDistance*[cos(sourceAng) -sin(sourceAng); sin(sourceAng) cos(sourceAng)]*[1 0]')';
            lastPoint = pEnd(1:2) + (obj.extraPointsDistance*[cos(targetAng) -sin(targetAng); sin(targetAng) cos(targetAng)]*[1 0]')';
            pDist = lastPoint - firstPoint;
            
            % Allowed directions of motion
            Up = [0 1]; Left = [-1 0]; Down = [0 -1]; Right = [1 0];
            
            % Initial direction
            distances = 0;
            if diagram.isWithin(mod(sourceAng-pi/4,2*pi), obj.quadrant1,'[)')
                obj.directionFlags{1} = 'Up';
                if ~isempty(obj.extraDisplacementsPercent)
                    distances(1) = obj.extraDisplacementsPercent(1)*abs(pDist(2));
                end
                middlePoints{1} = firstPoint + distances(1)*Up;
            elseif diagram.isWithin(mod(sourceAng-pi/4,2*pi), obj.quadrant2,'[)')
                obj.directionFlags{1} = 'Left';
                if ~isempty(obj.extraDisplacementsPercent)
                    distances(1) = -obj.extraDisplacementsPercent(1)*abs(pDist(1));
                end
                middlePoints{1} = firstPoint + distances(1)*Left;
            elseif diagram.isWithin(mod(sourceAng-pi/4,2*pi), obj.quadrant3,'[)')
                obj.directionFlags{1} = 'Down';
                if ~isempty(obj.extraDisplacementsPercent)
                    distances(1) = -obj.extraDisplacementsPercent(1)*abs(pDist(2));
                end
                middlePoints{1} = firstPoint + distances(1)*Down;
            elseif diagram.isWithin(mod(sourceAng-pi/4,2*pi), obj.quadrant4,'[)')
                obj.directionFlags{1} = 'Right';
                if ~isempty(obj.extraDisplacementsPercent)
                    distances(1) = obj.extraDisplacementsPercent(1)*abs(pDist(1));
                end
                middlePoints{1} = firstPoint + distances(1)*Right;
            end
            
            % Next directions...
            i = 1; Delta = lastPoint - middlePoints{1};
            while norm(Delta) > 1e-10
                i = i+1;
                RotateDirection(obj, Delta);
                switch obj.directionFlags{i}
                    case 'Up'
                        if ~isempty(obj.extraDisplacementsPercent) && (i <= length(obj.extraDisplacementsPercent))
                            distances(i) = obj.extraDisplacementsPercent(i)*abs(Delta(2));
                        else
                            distances(i) = abs(Delta(2));
                        end
                        middlePoints{i} = middlePoints{i-1} + distances(i)*Up;
                    case 'Left'
                        if ~isempty(obj.extraDisplacementsPercent) && (i <= length(obj.extraDisplacementsPercent))
                            distances(i) = obj.extraDisplacementsPercent(i)*abs(Delta(1));
                        else
                            distances(i) = abs(Delta(1));
                        end
                        middlePoints{i} = middlePoints{i-1} + distances(i)*Left;
                    case 'Down'
                        if ~isempty(obj.extraDisplacementsPercent) && (i <= length(obj.extraDisplacementsPercent))
                            distances(i) = obj.extraDisplacementsPercent(i)*abs(Delta(2));
                        else
                            distances(i) = abs(Delta(2));
                        end
                        middlePoints{i} = middlePoints{i-1} + distances(i)*Down;
                    case 'Right'
                        if ~isempty(obj.extraDisplacementsPercent) && (i <= length(obj.extraDisplacementsPercent))
                            distances(i) = obj.extraDisplacementsPercent(i)*abs(Delta(1));
                        else
                            distances(i) = abs(Delta(1));
                        end
                        middlePoints{i} = middlePoints{i-1} + distances(i)*Right;
                end
                Delta = lastPoint - middlePoints{i};
            end
            
            %Add last direction to the directionFlags sequence
            RotateDirection(obj, pEnd(1:2) - middlePoints{end});
            
            %Deletes lastPoint if it's inside a segment
            if strcmp(obj.directionFlags{end}, obj.directionFlags{end-1})
                middlePoints = middlePoints(1:end-1);
            end
            
        end
        
        % Selects next diretion of motion
        function RotateDirection(obj, Delta)
            % If last direction is Up or Down
            tol = 1e-10;
            if strcmp(obj.directionFlags{end},'Up') || strcmp(obj.directionFlags{end},'Down')
                if Delta(1) > tol
                    obj.directionFlags{end+1} = 'Right';
                elseif Delta(1) < -tol
                    obj.directionFlags{end+1} = 'Left';
                elseif (abs(Delta(1)) < tol) && Delta(2) > tol
                    obj.directionFlags{end+1} = 'Up';
                elseif (abs(Delta(1)) < tol) && Delta(2) < tol
                    obj.directionFlags{end+1} = 'Down';
                else                        
                    obj.directionFlags{end+1} = obj.directionFlags{end};
                end
            else %Left or Right
                if Delta(2) > tol
                    obj.directionFlags{end+1} = 'Up';
                elseif Delta(2) < -tol
                    obj.directionFlags{end+1} = 'Down';
                elseif abs(Delta(2) < tol) && Delta(1) > tol
                    obj.directionFlags{end+1} = 'Right';
                elseif abs(Delta(2) < tol) && Delta(1) < tol
                    obj.directionFlags{end+1} = 'Left';
                else                        
                    obj.directionFlags{end+1} = obj.directionFlags{end};
                end
            end
        end
        
        function Print(obj)
            hold on;
            
            %Initialization
            linePrintProperties = {'Color','LineStyle','LineWidth','Marker','MarkerSize','MarkerEdgeColor','MarkerFaceColor'};
            linePrintPropertiesValues = {obj.lineColor,obj.lineStyle,obj.lineWidth,obj.lineMarker,obj.lineMarkerSize,obj.lineMarkerEdgeColor,obj.lineMarkerFaceColor};
            
            %Compute points
            obj.directionFlags = [];
            obj.points = [obj.points(1) obj.points(end)];
            middlePoints = obj.ComputeMiddlePoints(obj.points{1}, obj.points{end}, obj.extraDisplacementsPercent);
            obj.points = [obj.points(1) middlePoints obj.points(end)];
                        
            %Change middle points if we need to use segments centers
            s = size(middlePoints);
            if (obj.useSegmentCenter || obj.isSmooth) && (s(2) > 1)
                centerPoints = obj.GetSegmentsCenters(middlePoints);
                middlePoints = [middlePoints(1) centerPoints middlePoints(end)];
            end
            obj.points = [obj.points(1) middlePoints obj.points(end)];
                      
            %Change middle points if corners have fillets
            if (obj.filletRadius~=0) && (~obj.isSmooth)
                obj.AddFillets;
            end
            
            %Print points and spline points
            s = size(obj.points);
            printPoints = zeros(2,s(2));
            for i = 1:s(2)
                printPoints(:,i) = obj.points{i}(1:2);
            end            
            middlePoints = obj.points(2:end-1);
            s = size(middlePoints);
            splinePoints = zeros(2,s(2));
            for i = 1:s(2)
                splinePoints(:,i) = middlePoints{i}(1:2)';
            end
            
            %Get source and target angles
            sourceAng = obj.points{1}(3);
            if ~obj.useSourceAng
                p1 = obj.points{1}(1:2);
                p2 = obj.points{2}(1:2);
                p = p2 - p1;
                sourceAng = atan2(p(2), p(1));
            end            
            targetAng = obj.points{end}(3);
            if ~obj.useTargetAng
                p1 = obj.points{end}(1:2);
                p2 = obj.points{end-1}(1:2);
                p = p2 - p1;
                targetAng = atan2(p(2), p(1));
            end
            
            %Print initial and final segments
            temp = plot([printPoints(1,1) printPoints(1,2)], [printPoints(2,1) printPoints(2,2)]); %print first line segment
            set(temp,linePrintProperties,linePrintPropertiesValues);
            
            temp = plot([printPoints(1,end) printPoints(1,end-1)], [printPoints(2,end) printPoints(2,end-1)]); %print last line segment
            set(temp,linePrintProperties,linePrintPropertiesValues);

            %Print line segments or spline
            if ~obj.isSmooth
                temp = plot(printPoints(1,:), printPoints(2,:));
                set(temp,linePrintProperties,linePrintPropertiesValues);
            else
                arrowSpline = obj.MakeArrowSpline(splinePoints,sourceAng,targetAng);                
                handles_before = findall(gca); %Little trick to get fnplt handle
                fnplt(arrowSpline,'g');
                fnplt_handle = setdiff(findall(gca), handles_before); %Little trick to get fnplt handle
                set(fnplt_handle, linePrintProperties, linePrintPropertiesValues);
            end
            
            % Print arrow heads
            obj.PrintArrowHeads();
            
            hold off;
        end
        
    end    
end
