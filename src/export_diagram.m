function export_diagram(varargin)

%export_diagram generates a pdf file of the diagram using export_fig
%script. Please find it in https://github.com/altmany/export_fig and add it
%to MATLAB path. The in argument of export_diagram is the filepath/name
%(ex: /home/guilherme/my_diagram). If there is no in argument, the default
%filepath/name is ./diagram

axis off;
grid off;
set(gcf,'color','w');

if isempty(varargin)
    filepath = 'diagram';
else
    filepath = varargin{1};
end

export_fig(filepath,'-pdf','-painters');

end