classdef Diagram2
    %DIAGRAM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        arrows
        blocks
        circles
        
        portFontSize
        portFontColor
        portHorizontalAlignment
        portVerticalAlignment
        
        blockTxt
        blockPos
        blockSize        
        blockFontSize
        blockFontColor
        blockBackgroundColor
        blockBorderColor
        blockBorderWidth
        blockBorderCurvature
        blockBorderLineStyle
        
        circleTxt
        circlePos
        circleRadius
        circleOffset
        circlePortGain
        circlePortOffset        
        circleFontSize
        circleFontColor
        circleBackgroundColor
        circleBorderColor
        circleBorderWidth
        circleBorderLineStyle
        
        arrowIsSmooth
        arrowUseExtraPoints
        arrowOffsetDistance
        arrowExtraPointsDistance
        arrowHeadBase
        arrowHeadHeight
        arrowHasSourceHead
        arrowHasTargetHead
        arrowUseSourceAng
        arrowUseTargetAng
        arrowUseSegmentCenter
        arrowFilletRadius
        arrowFilletResolution
        arrowLineColor        
        arrowHeadBackgroundColor
        arrowHeadFaceAlpha
        arrowLineStyle
        arrowLineWidth
        arrowLineMarker
        arrowLineMarkerSize
        arrowLineMarkerEdgeColor
        arrowLineMarkerFaceColor
    end
    
    methods
        function obj = Diagram2()
        end
    end

    methods(Static)
        
        % Initializes diagram properties.
        function Init()
            global diagram;
            diagram = Diagram2();
            
            diagram.portFontSize = 0.06;
            diagram.portFontColor = [0 0 0];
            diagram.portHorizontalAlignment = 'center';
            diagram.portVerticalAlignment = 'middle';
            
            diagram.blockTxt = '';
            diagram.blockPos = [0 0];
            diagram.blockSize = [1 1];
            diagram.blockFontSize = 0.08;
            diagram.blockFontColor = [0 0 0];
            diagram.blockBackgroundColor = 'none';
            diagram.blockBorderColor = [0 0 0];
            diagram.blockBorderWidth = 0.5;
            diagram.blockBorderCurvature = [0 0];
            diagram.blockBorderLineStyle = '-';
            
            diagram.circleTxt = diagram.blockTxt;
            diagram.circlePos = diagram.blockPos;
            diagram.circleRadius = 1;
            diagram.circleOffset = 0;
            diagram.circlePortGain = 0.8;
            diagram.circlePortOffset = 0;           
            diagram.circleFontSize = diagram.blockFontSize;
            diagram.circleFontColor = diagram.blockFontColor;
            diagram.circleBackgroundColor = diagram.blockBackgroundColor;
            diagram.circleBorderColor = diagram.blockBorderColor;
            diagram.circleBorderWidth = diagram.blockBorderWidth;
            diagram.circleBorderLineStyle = diagram.blockBorderLineStyle;
            
            diagram.arrowIsSmooth = false;
            diagram.arrowHeadBase = .025;
            diagram.arrowHeadHeight = 2*diagram.arrowHeadBase;
            diagram.arrowUseExtraPoints = true;
            diagram.arrowOffsetDistance = 0.03;
            diagram.arrowExtraPointsDistance = diagram.arrowHeadHeight+diagram.arrowOffsetDistance;
            diagram.arrowHasSourceHead = false;
            diagram.arrowHasTargetHead = true;
            diagram.arrowUseSourceAng = true;
            diagram.arrowUseTargetAng = true;            
            diagram.arrowUseSegmentCenter = false;
            diagram.arrowFilletRadius = 0;
            diagram.arrowFilletResolution = 20;
            diagram.arrowLineColor = 'black';        
            diagram.arrowHeadBackgroundColor = 'black';
            diagram.arrowHeadFaceAlpha = 1;
            diagram.arrowLineStyle = '-';
            diagram.arrowLineWidth = 0.5;
            diagram.arrowLineMarker = 'none';
            diagram.arrowLineMarkerSize = 6;
            diagram.arrowLineMarkerEdgeColor = [0 0 0];
            diagram.arrowLineMarkerFaceColor = 'none';
        end
        
        % Checks if x is within range vector.
        function output = isWithin(x, range, varargin)
            
            % Forbitten entry... returns false
            if range(1) >= range(2)
                output = false;
                return
            end
            
            % Specify interval type
            if nargin > 2
                intervalType = varargin{1};
            else
                intervalType = '()';
            end
            
            % Returns logical value 
            switch intervalType
                case '()'
                    if gt(x,range(1)) && lt(x,range(2))
                        output = true;
                        return
                    else
                        output = false;
                        return
                    end
                case '(]'
                    if gt(x,range(1)) && le(x,range(2))
                        output = true;
                        return
                    else
                        output = false;
                        return
                    end
                case '[)'
                    if ge(x,range(1)) && lt(x,range(2))
                        output = true;
                        return
                    else
                        output = false;
                        return
                    end
                case '[]'
                    if ge(x,range(1)) && le(x,range(2))
                        output = true;
                        return
                    else
                        output = false;
                        return
                    end
                otherwise
                    if gt(x,range(1)) && lt(x,range(2))
                        output = true;
                        return
                    else
                        output = false;
                        return
                    end
            end
        end
        
        % Prints diagram and all of its graphical objects.
        function Print()
            
            clf;
            
            global diagram;
            
            sBlocks = size(diagram.blocks);
            for i = 1:sBlocks(2)
                diagram.blocks{i}.PrintBlock();
            end
            
            sCircles = size(diagram.circles);
            for i = 1:sCircles(2)
                diagram.circles{i}.PrintCircle();
            end
            
            xLimits = get(gca,'XLim');
            yLimits = get(gca,'YLim');
            axis([xLimits*(eye(2) + [-1; 1]/2*0.2*[-1 1]) yLimits*(eye(2) + ([-1; 1]/2)*0.2*[-1 1])]);
            
            sArrows = size(diagram.arrows);
            for i = 1:sArrows(2)
                diagram.arrows{i}.Print();
            end            
            
            axis auto;         
            axis image;
            set(gcf,'color','w');
            grid on;
            
            for i = 1:sBlocks(2)
                diagram.blocks{i}.PrintText();
            end
            
            for i = 1:sCircles(2)
                diagram.circles{i}.PrintText();
            end
        end
    end
    
end

